package dev.anau.nameslut;

import at.mukprojects.giphy4j.Giphy;
import at.mukprojects.giphy4j.entity.search.SearchFeed;
import at.mukprojects.giphy4j.entity.search.SearchRandom;
import at.mukprojects.giphy4j.exception.GiphyException;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CustomEventHandler implements EventListener {

    @Override
    public void onEvent(GenericEvent event) {
        if (event instanceof ReadyEvent) {
            System.out.println("API is ready!");
            ReadyEvent e = (ReadyEvent) event;

            List<Member> members = e.getJDA().getGuildById("895346169683787807").getMembers();
            for(Member m: members){
                if(m.getEffectiveName().toLowerCase().contains(Main.getRealName(m.getIdLong()).toLowerCase())){
                    continue;
                }
                m.modifyNickname(m.getEffectiveName() + " ("+Main.getRealName(m.getIdLong())+")").queue();
            }
        }

        if(event instanceof GuildMemberUpdateNicknameEvent) {
            GuildMemberUpdateNicknameEvent e = (GuildMemberUpdateNicknameEvent) event;
            if(!e.getNewNickname().toLowerCase().contains(Main.getRealName(e.getEntity().getIdLong()).toLowerCase())){
                e.getEntity().modifyNickname(e.getNewNickname() + " ("+Main.getRealName(e.getEntity().getIdLong())+")").queue();
            }
        }


        if(event instanceof GuildMessageReceivedEvent) {
            GuildMessageReceivedEvent e = (GuildMessageReceivedEvent) event;
            Message m = e.getMessage();

            if(!e.getMember().getEffectiveName().toLowerCase().contains(Main.getRealName(e.getMember().getIdLong()).toLowerCase())){
                e.getMember().modifyNickname(e.getMember().getEffectiveName() + " ("+Main.getRealName(e.getMember().getIdLong())+")").queue();
                e.getMessage().reply("Oi! Keep your name in line, fucking twat.").queue();
            }


            if (!(m.getContentStripped().startsWith("!rename ") || m.getContentRaw().toLowerCase().startsWith("pls "))) {
                return; // We dont give a shit, its not a rename command
            }
            if (m.getContentStripped().startsWith("!rename ")) {
                String[] args = m.getContentRaw().split(" ");
                List<Member> members = e.getMessage().getMentionedMembers(e.getGuild());
                if (members.size() != 1 || args.length < 3) {
                    System.out.println("Args: " + args.length);
                    System.out.println("Mentions: " + members.size());
                    m.reply("Right, its not hard `!rename @user NewName` fucking hell").queue();
                    return; //I expect Simon will hit this use case 1st.
                }
                Member member = members.get(0);

                String[] args2 = args.clone();
                args2[0] = "";
                args2[1] = "";
                String newName  = String.join(" ", args2).trim();
                if(!newName.toLowerCase().contains(Main.getRealName(member.getIdLong()).toLowerCase())){
                    newName = newName+ " ("+Main.getRealName(member.getIdLong())+")";
                }


                member.modifyNickname(newName).queue();
                m.reply(member.getAsMention() + "'s nickname has been updated.").queue();
            }
            if( m.getContentRaw().toLowerCase().startsWith("pls ")){
                System.out.println("SEARCH GIPHY");
                String search = m.getContentStripped().toLowerCase().replaceFirst("pls ","");;

                Giphy giphy = new Giphy("VJUFkwJ5gFkMeKVwcpV6DhWmzDQajuiV");
                SearchRandom feed = null;

                try {
                    feed = giphy.searchRandom(search);
                } catch (GiphyException giphyException) {
                    MessageEmbed embed = new EmbedBuilder()
                            .setDescription("You sick fuck, I have no: "+search+"\n Or, I am broke.\n"+giphyException.getLocalizedMessage())
                            .setColor(Color.red)
                            .build();
                    e.getMessage().reply(embed).complete();
                    giphyException.printStackTrace();
                }
                if(feed.getData() == null){

                }
                String imgURL = feed.getData().getImageOriginalUrl();


                MessageEmbed embed = new EmbedBuilder()
                        .setImage(imgURL)
                        .setDescription("Here have your "+search)
                        .setColor(Color.cyan)
                        .build();
                e.getMessage().reply(embed).complete();
            }
        }
    }
}
