package dev.anau.nameslut;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Member;

import javax.security.auth.login.LoginException;
import java.util.HashMap;
import java.util.List;

public class Main {

    protected static HashMap<Long, String> realnameMapping;

    public static Main thisInstance;

    public JDA bot;

    public static void main(String[] args) throws Exception {
        Main.realnameMapping = new HashMap<>();

        Main.realnameMapping.put(160777385820684288l, "Andrew");
        Main.realnameMapping.put(229507506538414080l, "Erich");
        Main.realnameMapping.put(186799462914588673l, "Andrew");
        Main.realnameMapping.put(310496504160845827l, "Simon");
        Main.realnameMapping.put(895566386712174623l, "Robbie");
        Main.realnameMapping.put(140176382293573632l, "Zoey");
        Main.realnameMapping.put(509847078256640010l, "Roxy");
        Main.realnameMapping.put(142890590219010048L, "Sam");
        Main.realnameMapping.put(828046619252490241l, "Admin Account");
        Main.realnameMapping.put(215545165404700683l, "Louis");
        Main.realnameMapping.put(416544918828482560l, "Kev");
        Main.realnameMapping.put(707904024111742978l, "Sean");
        Main.realnameMapping.put(916231155366363178l, "Dan");
        Main.realnameMapping.put(384864456120860672l, "Andrius");
        Main.realnameMapping.put(926575859387752498l, "Tom");
        Main.realnameMapping.put(512245094032605184l, "Mike");
        // Pass in the bot token as argument one whe running the jar
        // java -jar bot.jar "TOKEN"
        if(args.length != 1){
            throw new Exception("Please provide an authentication token");
        }
        thisInstance = new Main(args[0]);

    }
    public static String getRealName(long userId){
        if(!Main.realnameMapping.containsKey(userId)){
            return "Unknown";
        }
        return Main.realnameMapping.get(userId);
    }
    public Main(String token) throws LoginException, InterruptedException {
       bot = JDABuilder
               .createDefault(token)
               .setActivity(Activity.watching("you fuckers"))
               .addEventListeners(new CustomEventHandler())
               .build();

       bot.awaitReady();



    }
}
